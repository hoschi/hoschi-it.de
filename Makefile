# make tasks not depend on existing output files
.PHONY: *

target = "distribution"

build: clean
	cd src/11ty && npm run build

deploy: build
	tools/deploy

clean:
	find $(target)/ -mindepth 1 | xargs -I % rm -Rf %

commit: build
	# commit the build
	git add .
	git commit -m "Automated build" \
	  --author="build script <script@hoschi-it.de>"
