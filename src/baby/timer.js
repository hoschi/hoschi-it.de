// Design by foolishdeveloper.com
// https://www.foolishdeveloper.com/2021/07/simple-countdown-timer-using-javascript.html

daysFactor = 1000 * 60 * 60 * 24;
future = Date.parse("dec 17, 2022 00:00:00");

function getElementWith(name, number) {
    return `<div>${number}<span>${name}</span></div>`;
}

function getWeeks(msec){
    return msec / (daysFactor * 7);
}

function updateTimer() {
    var now = new Date();
    var diff = future - now;

    var weeks = Math.floor(getWeeks(diff));
    var days =  Math.ceil( (diff - (weeks * 7 * daysFactor) ) / daysFactor);

    document.getElementById("timer").innerHTML = getElementWith("Wochen", weeks) + getElementWith("Tage", days);
}

updateTimer();
setInterval('updateTimer()', 10000);
