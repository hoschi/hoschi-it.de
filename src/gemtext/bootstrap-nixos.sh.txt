#!/bin/sh
# Get the dotfiles repo

# Props to https://dev.to/vibioh/dotfiles-5695

# make it exit if anything fails
set -o nounset 
set -o pipefail
set -o errexit

# print commands before executing them
set -o xtrace 

main() {
    local INSTALL_PATH="${HOME}/Repos"
    local GIT_SERVER='https://app.hoschi-it.de/gitea'
    local GIT_USER='janina'
    local DOTFILES_REPO="NixOS-config"
    local DOTFILES_DOWNLOAD_URL=${GIT_SERVER}/${GIT_USER}/${DOTFILES_REPO}.git
    local TEMPORARY_ARCHIVE_FILE="/tmp/dotfiles.tar.gz"

    mkdir -p $INSTALL_PATH
    cd $INSTALL_PATH

    nix-shell -p 'git' \
        --command "git clone $DOTFILES_DOWNLOAD_URL ${DOTFILES_REPO,,}"

    set +o xtrace # turn of verbosity

    echo "Placed $DOTFILES_REPO at $(pwd)/${DOTFILES_REPO,,}"
    echo "Have fun!"
}

main
