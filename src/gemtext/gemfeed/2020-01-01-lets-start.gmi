# Hello world!
on 2020-01-01

When you are excited about something, you want to tell the world, right?

You want to show them, how awesome this stuff is, those silly but funny things you can do with it, all the details that you are so passionate about.

But maybe, they won’t agree.

“This is so geeky!”, “I don’t understand a thing.”, “It’s so boring...""

And when this is the case, for peace sake, you will stop telling others one day. This is dangerous because that’s the point where your passion begins to crumble. What else should you be talking about?

That is the point where I was about one or two years ago. I realized just a few days ago. So starting this blog I will take you through my everyday tech experiments hoping, someone will read it and be inspired.

Even if you are the only one reading this: That’s worth it for me.

They call me hoschi and I want to rouse my passion. Will you join me?

---
=> ./ logs
=> ../ 🏡 home
