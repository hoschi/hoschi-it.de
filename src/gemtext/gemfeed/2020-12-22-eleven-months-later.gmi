# 11 months later
on 2020-12-22

11 months later and I haven’t written the second post yet. Seems like life got harder and all that power I once had just vanished. But due to the fact that I am now writing again it could be the a new beginning for the website whose domain adress I already dismissed once.

So let’s get into it!

### A new start 🙭 - here is why

A few days ago I read about the so-called IndieWeb, which I find very interesting but also kind of shadowy.

It is about not saving all your thoughts and identity into silos that big web companies offer you but putting them onto your own webpage and then giving the silos a link to that thoughts. This way, even if a company changes their terms of services to not longer be freely available or disappears into the depths of history, all those thoughts will be under your control, even if some comments from social media about that may be gone.

At least, that is how I understood it. So if you now better, please correct me!

So with the IndieWeb in mind it hit me: I have my own website. Even if it has not been in use for a few months now, but for the sake of learning who I am and how to express myself I decided to give it a go. 😁

=> https://indieweb.org/ Indieweb

---
=> ./ logs
=> ../ 🏡 home
