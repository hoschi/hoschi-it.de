# 💡 Inspirationsquellen

Hier sind einige Quellen die ich als hilfreich empfunden habe. Ein paar davon beeinflussen bis heute wie ich denke und handle. Ich hoffe, sie sind auch für Dich hilfreich.

 *Disclaimer*
Dies ist keine Werbung sondern eine Empfehlung rein privater Natur.
Für die Bücher habe ich mich entschieden, keine Links anzugeben, die doch häufig veralten. Stattdessen nutze man bei Interesse die Buchhandlung oder Suchmaschine seiner Wahl.

## Geistlichkeit

* Neues Leben Bibel
* Gott ist nicht tot - Warum alles dafür spricht, dass es Gott gibt; Rice Broocks
* Tochter Gottes, erhebe dich - Vom Schmerz zum Sieg. Vom Sieg zum Segen.; Inka Hammond
* 10.000 Gründe - Geschichten von Hoffnung, Wundern und verwandelten Herzen; Matt Redman
=> https://www.bibleserver.com/ Online Bibel lesen

## Software-Entwicklung

* The Pracmatic Programmer; David Thomas, Andrew Hunt
* The twelve-factor app; Adam Wiggins

## Sonstige

* simplify your life: Einfacher und glücklicher leben; Werner Tiki Küstenmacher
* Die Känguru-Chroniken; Marc-Uwe Kling

---
=> ./ 🏡 home
