# Schwangerschaft

## Android Widget der Schwangerschaftswoche

Es gibt einige Apps, die sich rund um Themen der Schwangerschaft drehen.
Dazwischen ist auch einiges unnützes, was dem Nutzer ganz viele Daten entlockt.
Ich wollte "einfach nur" wissen, in welcher Schwangerschaftswoche ich mich
mittlerweile befinde.

Hierfür wird landläufig das Schema
```
SSW<n>+<m>
```
verwendet , wobei "n" für die Anzahl der Wochen seit dem Begin der letzten Regelblutung steht, und "m" angibt, vor wie vielen Tagen es auf den Tag genau "n" Wochen her ist.
SSW steht für "Schwangerschaftswoche".

Zum Berechnen dieser Daten lässt sich die App "Progress Bars" zweckentfremden.

=> https://github.com/mattvchandler/progressbars Quellcode
=> https://f-droid.org/packages/org.mattvchandler.progressbars/ in F-Droid
=> https://play.google.com/store/apps/details?id=org.mattvchandler.progressbars im Google Play Store

Erstelle ein neues Widget für diese App mit den folgenden Einstellungen:

* Timer title: SSW
* Seperate start and end times: Nein
* Date: Der erste Tag deiner letzten aufgetretenen Periode
* Time: vorzugsweise Mitternacht
* Stop Countdown when event is reached: Nein
* Show event time: Nein
* Units of time to display: Wähle "Weeks" und "Days" aus
* Countdown messages: mache den Post-Event-Text leer oder zu "SSW "

Vergiss nicht, auf "Speichern" zu drücken.
Das wars schon. 👩

Nun solltest du ein Widget haben, das dir etwa das Folgende anzeigt:
> SSW
> <n> weeks and <m> days

---
=> ../ 🏡 home
