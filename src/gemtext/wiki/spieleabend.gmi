## Online Spieleabend

Spieleabende mit Freunden über weite Distanzen sind nicht immer einfach zu organisieren. Wenn man denoch gerne zusammen spielt, auch am PC, sind online Alternativen zu Brettspielen in bekannter Runde ganz nett.
Ein Voicechat^1 währenddessen hilft der Atmosphäre auf die Sprünge.

### Spiele

Spiele, die ohne Account nutzbar sind:

=> https://garticphone.com/de Gartic Phone
=> https://stadtlandflussonline.net Stadt-Land-Fluss
=> https://skribbl.io/ Skribbl, alias Montagsmaler
=> https://www.codenames.game/ Codenames
=> https://colonist.io/ Colonist, eine Nachahmung von Siedler von Catan
=> https://buddyboardgames.com/ 🇬🇧 Spielesammlung "buddyboardgames":
=> https://buddyboardgames.com/wizard  ├── Wizard
=> https://buddyboardgames.com/uno     ├── Uno
=> https://buddyboardgames.com/yahtzee └── Yahtzee aka. Kniffel

Spiele, die nur mit einem Account pro Spieler nutzbar sind:

=> https://www.brettspielwelt.de/Spiele/ Spielesammlung "brettspielwelt"
=> https://www.brettspielwelt.de/Spiele/BlackDog/       ├── Black Dog
=> https://www.brettspielwelt.de/Spiele/KingdomBuilder/ ├── Kingdom Builder
=> https://www.brettspielwelt.de/Spiele/NochMal/        └── Noch Mal!
=> https://www.youtube.com/watch?v=hZseKx5fuKs Brettspielwelt.de: Anleitung um ein Spiel mit Freunden zu starten

---
=> ./chat.gmi [1] Optionen für Voicechat und Videochat
=> ../ 🏡 home
